package pl.codementors.zoo.animals;


public class Animal implements Comparable<Animal> {

    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Animal= [name= " + name + "]";
    }

    @Override
    public int compareTo(Animal animal) {
        int rel = getName().compareTo(animal.getName());
        if (rel == 0) {
            return getClass().getCanonicalName().compareTo(getClass().getCanonicalName());
        }
        return rel;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Animal)) {
            return false;
        }
        Animal animal = (Animal) obj;
        return name.equals(animal.getName());
    }

    @Override
    public int hashCode() {
        return 37 * name.hashCode();
    }
}
