package pl.codementors.zoo.animals;

import java.util.Comparator;

/**
 * Created by pskiba on 13.06.2017.
 */
public class Sort implements Comparator <Animal> {


    @Override
    public int compare(Animal animal1, Animal animal2) {
        int rel = animal1.getClass().getSimpleName().compareTo(animal2.getClass().getSimpleName());
        if (rel == 0) {
            return animal1.getName().compareTo(animal2.getName());
        }
        return rel;
    }
}


