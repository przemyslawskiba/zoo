package pl.codementors.zoo.animals;

/**
 * Created by pskiba on 13.06.2017.
 */
public class Tiger extends Mammal {


    public Tiger(String name, String furColor) {
        super(name, furColor);
    }

    @Override
    public String toString() {
        return "Tiger= " + super.toString();
    }
}
