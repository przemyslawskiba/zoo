package pl.codementors.zoo.animals;

/**
 * Just a bird with feathers.
 *
 * @author psysiu
 */
public class Bird extends Animal {

    /**
     * Color of the bird feathers.
     */
    private String featherColor;

    public Bird(String name, String featherColor) {
        super(name);
        this.featherColor = featherColor;
    }

    @Override
    public String toString() {
        return "Bird= featherColor= " + featherColor + " " + super.toString();
    }

    public String getFeatherColor() {
        return featherColor;
    }

    public void setFeatherColor(String featherColor) {
        this.featherColor = featherColor;
    }

    @Override
    public int hashCode() {
        return 31 * getName().hashCode() + 21 * getFeatherColor().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Bird)) {
            return false;
        }
        Bird bird = (Bird) obj;
        return getName().equals(bird.getName()) && featherColor.equals(bird.getFeatherColor());
    }


    @Override
    public int compareTo(Animal o) {
        int ret = super.compareTo(o);
        if (ret == 0) {
            if (o instanceof Bird) {
                return featherColor.compareTo(((Bird) o).getFeatherColor());
            }
        }
        return ret;
    }
}
