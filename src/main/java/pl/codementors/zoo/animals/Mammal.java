package pl.codementors.zoo.animals;

public class Mammal extends Animal {

    /**
     * Color of the animal fur.
     */
    private String furColor;

    public Mammal(String name, String furColor) {
        super(name);
        this.furColor = furColor;
    }

    @Override
    public String toString() {
        return "Mammal= furColor= " + furColor + " " + super.toString();
    }


    public String getFurColor() {
        return furColor;
    }

    public void setFurColor(String furColor) {
        this.furColor = furColor;
    }

    @Override
    public int hashCode() {
        return 31 * getName().hashCode() + 21 * getFurColor().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Mammal)) {
            return false;
        }
        Mammal mammal = (Mammal) obj;
        return getName().equals(mammal.getName()) && furColor.equals(mammal.getFurColor());
    }

    @Override
    public int compareTo(Animal o) {
        int ret = super.compareTo(o);
        if (ret == 0) {
            if (o instanceof Mammal) {
                return furColor.compareTo(((Mammal) o).getFurColor());
            }
        }
        return ret;
    }
}

