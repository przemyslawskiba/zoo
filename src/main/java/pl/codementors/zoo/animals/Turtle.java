package pl.codementors.zoo.animals;

/**
 * Created by pskiba on 13.06.2017.
 */
public class Turtle extends Lizard {


    public Turtle(String name, String scalesColor) {
        super(name, scalesColor);
    }

    @Override
    public String toString() {
      return "Turtle= " + super.toString();
    }
}
