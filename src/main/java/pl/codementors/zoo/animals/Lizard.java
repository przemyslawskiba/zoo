package pl.codementors.zoo.animals;

/**
 * Lizard with some scales.
 *
 * @author psysiu
 */
public class Lizard extends Animal {

    /**
     * Color of the animal scales.
     */
    private String scalesColor;

    public Lizard(String name, String scalesColor) {
        super(name);
        this.scalesColor = scalesColor;
    }

    @Override
    public String toString() {
        return "Lizard= scalesColor= " + scalesColor + " " + super.toString();
    }


    public String getScalesColor() {
        return scalesColor;
    }

    public void setScalesColor(String scalesColor) {
        this.scalesColor = scalesColor;
    }


    @Override
    public int hashCode() {
        return 31 * getName().hashCode() + 21 * getScalesColor().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Lizard)) {
            return false;
        }
        Lizard lizard = (Lizard) obj;
        return getName().equals(lizard.getName()) && scalesColor.equals(lizard.getScalesColor());
    }

    @Override
    public int compareTo(Animal o) {
        int ret = super.compareTo(o);
        if (ret == 0) {
            if (o instanceof Lizard) {
                return scalesColor.compareTo(((Lizard) o).getScalesColor());
            }
        }
        return ret;
    }
}

