package pl.codementors.zoo;

import pl.codementors.zoo.animals.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;


public class ZooMain {

    /**
     * Application starting method.
     *
     * @param args Application starting params.
     */
    public static void main(String[] args) {

        List<Animal> animals = new ArrayList<>();

        try (InputStream is = ZooMain.class.getResourceAsStream("/pl/codementors/zoo/input/animals.txt");
             Scanner scanner = new Scanner(is);) {
            while (scanner.hasNext()) {
                String type = scanner.next();
                String name = scanner.next();
                String color = scanner.next();

                switch (type) {
                    case "Bird": {
                        animals.add(new Bird(name, color));
                        break;
                    }
                    case "Lizard": {
                        animals.add(new Lizard(name, color));
                        break;
                    }
                    case "Mammal": {
                        animals.add(new Mammal(name, color));
                        break;
                    }
                    case "Tiger": {
                        animals.add(new Tiger(name, color));
                        break;
                    }
                    case "Turtle": {
                        animals.add(new Turtle(name, color));
                        break;
                    }
                }
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }

        System.out.println(animals.equals(animals));
        Collections.sort(animals, new Sort());

        print(animals);

        Set<Animal> animalsSet = new TreeSet<>(new Sort());
        animalsSet.addAll(animals);
        print(animalsSet);
        System.out.println(animalsSet.size());
    }


    public static void print(Collection<Animal> animals) {
        for (Animal animal : animals) {
            System.out.println(animal);
        }
    }
}
